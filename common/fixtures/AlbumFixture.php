<?php

namespace common\fixtures;

use common\components\ActiveFixture;

class AlbumFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Album';

    public $depends = [
        'common\fixtures\PhotoFixture',
    ];
}