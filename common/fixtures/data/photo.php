<?php

return [
    [
        'id' => '1',
        'album_id' => '1',
        'title' => 'Photo 1',
    ],
    [
        'id' => '2',
        'album_id' => '1',
        'title' => 'Photo 2',
    ],
    [
        'id' => '3',
        'album_id' => '1',
        'title' => 'Photo 3',
    ],
    [
        'id' => '4',
        'album_id' => '1',
        'title' => 'Photo 4',
    ],
    [
        'id' => '5',
        'album_id' => '1',
        'title' => 'Photo 5',
    ],
];
