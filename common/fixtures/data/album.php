<?php

return [
    [
        'id' => 1,
        'user_id' => 1,
        'title' => 'Album 1',
    ],
    [
        'id' => 2,
        'user_id' => 1,
        'title' => 'Album 2',
    ],
    [
        'id' => 3,
        'user_id' => 1,
        'title' => 'Album 3',
    ],
];
