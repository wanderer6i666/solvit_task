<?php

namespace common\fixtures;

use common\components\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = 'common\models\User';

    public $depends = [
        'common\fixtures\AlbumFixture',
    ];
}
