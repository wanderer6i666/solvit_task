<?php

namespace common\fixtures;

use common\components\ActiveFixture;

class PhotoFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Photo';
}