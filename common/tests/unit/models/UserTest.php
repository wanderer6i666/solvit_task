<?php

namespace common\tests\unit\models;

use common\fixtures\UserFixture;
use common\models\Album;
use common\models\User;
use common\tests\UnitTester;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    public function _fixtures(): array
    {
        return ['users' => UserFixture::class];
    }

    public function testGetAlbums(): void
    {
        /** @var User $user */
        $user = User::find()
            ->with(['albums'])
            ->where(['id' => 1])
            ->one();

        // Check count
        $this->assertCount(3, $user->albums);
        // Check instances
        $this->assertContainsOnlyInstancesOf(Album::class, $user->albums);
    }
}