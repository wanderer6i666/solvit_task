<?php

namespace common\tests\unit\models;

use common\fixtures\PhotoFixture;
use common\models\Photo;
use common\tests\UnitTester;

class PhotoTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    public function _fixtures(): array
    {
        return ['photos' => PhotoFixture::class];
    }

    public function testGetUrl(): void
    {
        /** @var Photo $photo */
        $photo = Photo::findOne(1);

        // Check url is not empty
        $this->assertNotEmpty($photo->url);
    }
}