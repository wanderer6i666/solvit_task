<?php

namespace common\models;

use Exception;
use yii\db\ActiveQuery;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "photo".
 *
 * @property int $id
 * @property int $album_id
 * @property string $title
 * @property string $url
 *
 * @property Album $album
 */
class Photo extends \yii\db\ActiveRecord
{
    private string $url;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%photo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['album_id', 'title'], 'required'],
            [['album_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::class, 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->url = $this->generateUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'title' => 'Title',
        ];
    }

    /**
     * Gets query for [[Album]].
     *
     * @return ActiveQuery
     */
    public function getAlbum(): ActiveQuery
    {
        return $this->hasOne(Album::class, ['id' => 'album_id']);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    protected function generateUrl(): string
    {
        try {
            $photos = FileHelper::findFiles('@frontend/web/img/photo/', ['only' => ['*.jpg', '*.jpeg', '*.png']]);
            $photo = basename($photos[array_rand($photos)]);
            $url = Url::to('@web/img/photo/' . $photo, true);
        } catch (Exception $e) {

        }

        $url ??= 'https://via.placeholder.com/150';

        return $url;
    }
}
