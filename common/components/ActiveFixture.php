<?php

namespace common\components;

/**
 * Отключение проверки целостности вторичных ключей
 */
class ActiveFixture extends \yii\test\ActiveFixture
{
    public function beforeLoad()
    {
        parent::beforeLoad();
        $this->db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
    }

    public function afterLoad()
    {
        parent::afterLoad();
        $this->db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
    }
}
