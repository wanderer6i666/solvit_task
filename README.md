Clone project
-------------------
```
git clone https://wanderer6i666@bitbucket.org/wanderer6i666/solvit_task.git
```
```
cd solvit_task

```
-------------------

Setup
-------------------
```
composer install
php init
```
Select [0] Development environment

Create DB and test DB. Change DB settings in
```
common/main-local.php
common/test-local.php
```
Set password for generated users in
```
console/params-local.php
```
```
return [
    'usersPassword' => 'verysecret', // Should be set in params-local.php
];
```
Run migrations
```
php yii migrate
php yii_test migrate
```
Run tests
```
./vendor/bin/codecept run
```
Run users generator
```
php yii user-generator/generate
```
Configure webserver url to 'api' application

API endpoint url example
```
http://solvit-task.local/v1/users
http://solvit-task.local/v1/users/1
http://solvit-task.local/v1/albums
http://solvit-task.local/v1/albums/1
```

-------------------

Setup with docker
-------------------
```
docker-compose build --no-cache 
docker-compose up -d
docker-compose exec php-fpm bash
```
```
composer install
php init
```
Select [0] Development environment

Create DB and test DB. Change DB settings in
```
common/main-local.php
common/test-local.php
```
Copy from
```
common/main-local-exaample-for-docker.php
common/test-local-exaample-for-docker.php
```
Set password for generated users in 
```
console/params-local.php
```
```
return [
    'usersPassword' => 'verysecret', // Should be set in params-local.php
];
```
Run migrations
```
php yii migrate
php yii_test migrate
```
Run tests
```
./vendor/bin/codecept run
```
Run users generator
```
php yii user-generator/generate
```
API endpoint url example
```
http://localhost:47000/v1/users
http://localhost:47000/v1/users/1
http://localhost:47000/v1/albums
http://localhost:47000/v1/albums/1
```