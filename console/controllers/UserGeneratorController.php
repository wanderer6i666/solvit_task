<?php

namespace console\controllers;

use console\components\AlbumGenerator;
use console\components\PhotoGenerator;
use console\components\UserGenerator;
use Yii;

class UserGeneratorController extends \console\components\Controller
{
    public function actionIndex(): void
    {
        echo $this->out('yii user-generator/generate');
    }

    public function actionGenerate(int $count = 10): void
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $userGenerator = new UserGenerator($count);
            $albumsGenerator = new AlbumGenerator(null, 100);
            $photoGenerator = new PhotoGenerator(null, 10000);
            $userGenerator->setNextGenerator($albumsGenerator)->setNextGenerator($photoGenerator);

            $userGenerator->execute();

            $transaction->commit();

            echo $this->out("Generated {$count} users");
        } catch (\Exception $e) {
            $transaction->rollBack();
            echo $this->out($e->getMessage());
        }
    }
}
