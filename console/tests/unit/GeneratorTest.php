<?php

namespace console\tests\unit\models;

use api\models\Album;
use common\fixtures\UserFixture;
use common\models\User;
use console\components\AlbumGenerator;
use console\components\PhotoGenerator;
use console\components\UserGenerator;
use console\tests\UnitTester;

class GeneratorTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    public function _fixtures(): array
    {
        return [
            'users' => UserFixture::class,
        ];
    }

    public function testGenerateUsers(): void
    {
        $generator = new UserGenerator(10);
        $users = $generator->execute();

        $this->assertCount(10, $users);
    }

    public function testGenerateAlbums(): void
    {
        $user = User::findOne(1);
        $generator = new AlbumGenerator($user, 10);
        $albums = $generator->execute();

        $this->assertCount(10, $albums);
    }

    public function testGeneratePhotos(): void
    {
        $album = Album::findOne(1);
        $generator = new PhotoGenerator($album, 10);
        $photoCount = $generator->execute();

        $this->assertEquals(10, $photoCount);
    }

    public function testGenerateUsersAlbumsPhotos(): void
    {
        $userGenerator = new UserGenerator(10);
        $albumsGenerator = new AlbumGenerator(null, 100);
        $photoGenerator = new PhotoGenerator(null, 1000);


        $userGenerator->setNextGenerator($albumsGenerator)->setNextGenerator($photoGenerator);
        $users = $userGenerator->execute();

        $this->assertCount(10, $users);
    }
}