<?php


namespace console\components;

use yii\db\ActiveRecord;

abstract class AbstractGenerator implements Generator
{
    public int $count;

    protected string $parentClass;

    protected string $modelClass;

    protected array $models = [];

    protected ?ActiveRecord $parentModel = null;

    protected ?Generator $nextGenerator = null;

    public function __construct(?ActiveRecord $parentModel = null, int $count = 100)
    {
        if ($count <= 0) {
            throw new \Exception('Invalid argument');
        }

        $this->count = $count;

        if ($parentModel) {
            $this->setParentModel($parentModel);
        }
    }

    public function execute()
    {
        for ($i = 1; $i <= $this->count; $i++) {
            $model = $this->generateModel();

            if ($nextGenerator = $this->getNextGenerator()) {
                $nextGenerator->setParentModel($model);
                $nextGenerator->execute();
            }

            $this->models[] = $model;
        }

        return $this->models;
    }

    public function getModels(): array
    {
        return $this->models;
    }

    public function setNextGenerator(Generator $generator): Generator
    {
        $this->nextGenerator = $generator;

        return $generator;
    }

    public function getNextGenerator(): ?Generator
    {
        return $this->nextGenerator;
    }

    public function setParentModel(?ActiveRecord $model): void
    {
        if (!($model instanceof $this->parentClass) || $model->isNewRecord) {
            throw new \Exception('Invalid argument');
        }

        $this->parentModel = $model;
    }

    public function getParentModel(): ?ActiveRecord
    {
        return $this->parentModel;
    }

    abstract public function generateModel();
}