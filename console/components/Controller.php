<?php

namespace console\components;

use yii\helpers\Console;

class Controller extends \yii\console\Controller
{
    /**
     * Print message with line break
     *
     * @param string $string
     *
     * @return bool|int
     */
    public function out(string $string): bool|int
    {
        return Console::output($string);
    }
}
