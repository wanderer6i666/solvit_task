<?php

namespace console\components;

use common\models\Album;
use common\models\User;
use Yii;
use yii\base\Exception;

class AlbumGenerator extends AbstractGenerator
{
    protected string $parentClass = User::class;
    protected string $modelClass = Album::class;

    /**
     * @throws \yii\base\Exception
     */
    public function generateModel(): Album
    {
        /** @var Album $model */
        $model = new $this->modelClass();

        $randStr = Yii::$app->security->generateRandomString(rand(5, 20));
        $data = [
            'user_id' => $this->getParentModel()->id,
            'title' => 'Title' . $randStr,
        ];

        $model->setAttributes($data);

        if (!$model->validate() || !$model->save()) {
            throw new Exception(print_r($model->errors, true));
        }

        return $model;
    }
}
