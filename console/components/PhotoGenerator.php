<?php

namespace console\components;

use common\models\Album;
use common\models\Photo;
use Yii;

class PhotoGenerator extends AbstractGenerator
{
    protected string $parentClass = Album::class;
    protected string $modelClass = Photo::class;

    /**
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     */
    public function execute(): int
    {
        $data = [];
        $albumId = $this->parentModel->id;

        for ($i = 1; $i <= $this->count; $i++) {
            $randStr = Yii::$app->security->generateRandomString(rand(5, 20));
            $data[] = [
                $albumId,
                "Photo {$randStr}",
            ];
        }

        return Yii::$app->getDb()->createCommand()->batchInsert(
            Photo::tableName(),
            ['album_id', 'title'],
            $data
        )->execute();
    }

    public function generateModel()
    {
    }
}
