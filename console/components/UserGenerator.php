<?php

namespace console\components;

use common\models\User;
use Yii;
use yii\base\Exception;

class UserGenerator extends AbstractGenerator
{
    protected string $modelClass = User::class;

    /**
     * @throws \Exception
     */
    public function __construct(int $count = 10)
    {
        return parent::__construct(null, $count);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateModel(): User
    {
        /** @var User $model */
        $model = new $this->modelClass();

        $randStr = Yii::$app->security->generateRandomString(rand(5, 20));
        $data = [
            'username' => $randStr,
            'first_name' => 'Firstname' . $randStr,
            'last_name' => 'Lastname' . $randStr,
            'email' => $randStr . '@schultz.info',
            'status' => User::STATUS_ACTIVE,
        ];

        $password = Yii::$app->params['usersPassword'];

        if (empty($password)) {
            throw new \Exception('Unconfigured param "usersPassword" could not be empty');
        }

        $model->setAttributes($data, false);
        $model->setPassword($password);
        $model->generateAuthKey();
        $model->generatePasswordResetToken();
        $model->generateEmailVerificationToken();

        if (!$model->validate() || !$model->save()) {
            throw new Exception(print_r($model->errors, true));
        }

        return $model;
    }
}
