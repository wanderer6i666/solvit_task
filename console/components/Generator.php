<?php

namespace console\components;

use yii\db\ActiveRecord;

/**
 * @property int $count
 * @property string $parentClass
 * @property string $modelClass
 * @property array $models
 * @property ActiveRecord|null $parentModel
 * @property Generator|null $nextGenerator
 */
interface Generator
{
    public function execute();

    public function getModels(): array;

    public function setNextGenerator(Generator $generator): Generator;

    public function getNextGenerator(): ?Generator;

    public function setParentModel(?ActiveRecord $model): void;

    public function getParentModel(): ?ActiveRecord;

    public function generateModel();
}