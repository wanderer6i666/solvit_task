<?php

use yii\db\Migration;

/**
 * Class m220320_142712_alter_user_table
 */
class m220320_142712_alter_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'last_name', $this->string()->after('username'));
        $this->addColumn('{{%user}}', 'first_name', $this->string()->after('username'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'last_name');
    }
}
