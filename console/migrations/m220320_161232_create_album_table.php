<?php

use common\components\Migration;

/**
 * Handles the creation of table `{{%album}}`.
 */
class m220320_161232_create_album_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%album}}', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('idx-user_id-album', '{{%album}}', 'user_id');
        $this->addForeignKey('fk-user_id-album', '{{%album}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%album}}');
    }
}
