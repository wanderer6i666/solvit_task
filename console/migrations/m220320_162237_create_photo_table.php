<?php

use common\components\Migration;

/**
 * Handles the creation of table `{{%photo}}`.
 */
class m220320_162237_create_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'album_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string()->notNull(),
        ], $this->tableOptions);

        $this->createIndex('idx-album_id-photo', '{{%photo}}', 'album_id');
        $this->addForeignKey('fk-album_id-photo', '{{%photo}}', 'album_id', '{{%album}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%photo}}');
    }
}
