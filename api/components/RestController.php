<?php

namespace api\components;

use Yii;

class RestController extends \yii\rest\ActiveController
{
    public $modelClass;
    public $searchModelClass;

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new $this->searchModelClass();

        return $searchModel->search(Yii::$app->request->queryParams);
    }
}
