<?php

namespace api\models;

use yii\db\ActiveQuery;

class Album extends \common\models\Album
{
    public function fields()
    {
        return [
            'id',
            'title',
            'photos',
        ];
    }

    /**
     * Gets query for [[Photo]].
     *
     * @return ActiveQuery
     */
    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['album_id' => 'id']);
    }
}
