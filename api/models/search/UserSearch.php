<?php

namespace api\models\search;

use api\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function fields()
    {
        return ['id', 'first_name', 'last_name'];
    }

    public function rules(): array
    {
        return [];
    }

    public function scenarios(): array
    {
        return Model::scenarios();
    }

    public function search(array $params)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
