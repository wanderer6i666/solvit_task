<?php

namespace api\models\search;

use api\models\Album;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AlbumSearch extends Album
{
    public function fields()
    {
        return ['id', 'title'];
    }

    public function rules(): array
    {
        return [];
    }

    public function scenarios(): array
    {
        return Model::scenarios();
    }

    public function search(array $params)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
