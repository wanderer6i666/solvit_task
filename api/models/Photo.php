<?php

namespace api\models;

class Photo extends \common\models\Photo
{
    public function fields()
    {
        return [
            'id',
            'title',
            'url',
        ];
    }
}
