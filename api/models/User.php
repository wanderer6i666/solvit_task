<?php

namespace api\models;

class User extends \common\models\User
{
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'albums' => function (User $owner) {
                return $owner->getAlbums()->select(['id', 'title'])->all();
            },
        ];
    }
}
