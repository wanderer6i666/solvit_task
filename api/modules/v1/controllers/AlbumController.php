<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use api\models\Album;
use api\models\search\AlbumSearch;

/**
 * Album controller for the `v1` module
 */
class AlbumController extends RestController
{
    public $modelClass = Album::class;
    public $searchModelClass = AlbumSearch::class;
}
