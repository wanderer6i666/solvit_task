<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use api\models\search\UserSearch;
use api\models\User;

/**
 * User controller for the `v1` module
 */
class UserController extends RestController
{
    public $modelClass = User::class;
    public $searchModelClass = UserSearch::class;
}
